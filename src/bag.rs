extern crate rand;

use self::rand::{thread_rng, Rng};

use super::tile::Color;

pub struct Bag {
    tiles: Vec<Color>,
}

impl Bag {
    pub fn new() -> Self {
        let mut bag = Bag { tiles: vec![] };
        bag.add_color(Color::Black);
        bag.add_color(Color::Blue);
        bag.add_color(Color::Red);
        bag.add_color(Color::Green);
        bag.add_color(Color::Yellow);

        {
            let slice = &mut bag.tiles;
            thread_rng().shuffle(slice);
        }

        bag
    }

    pub fn not_empty(&self) -> bool {
        return !self.tiles.is_empty();
    }

    pub fn pop(&mut self) -> Option<Color> {
        self.tiles.pop()
    }

    fn add_color(&mut self, color: Color) {
        for _ in 0..20 {
            self.tiles.push(color.clone());
        }
    }
}
