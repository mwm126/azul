use std::io::{self, BufRead};

use super::tile::Color;
use super::Game;

pub struct Turn {
    pub factory_index: usize,
    pub color: Color,
    pub line_index: usize,
}

pub fn read_number_of_players() -> u8 {

    loop {
        println!("One player, or two? (type 1 or 2):  ");
        let stdin = io::stdin();
        let line = stdin.lock().lines().next().unwrap().unwrap();
        match line.as_ref() {
            "1" => return 1,
            "2" => return 2,
            _ => continue,
        }
    }
}

pub fn read_turn(game: &Game, board_index: usize) -> Turn {
    loop {
        println!("Player {} move:  ", board_index + 1);
        let stdin = io::stdin();
        let line = stdin.lock().lines().next().unwrap().unwrap();

        if line.contains("q") {
            ::std::process::exit(0x0);
        }

        if line.len() != 3 {
            println!("Each move must be 3 characters:  digit, color, digit");
            continue
        }
        match parse_chars(&game, &line, board_index) {
            Err(msg) => {
                println!("{}", msg);
                continue;
            }

            Ok(turn) => match turn.is_legal(game, board_index) {
                Err(msg) => {
                    println!("{}", msg);
                    continue;
                }
                Ok(()) => {
                    println!(
                        "Move {} tiles from Factory {} to line {}",
                        turn.color, turn.factory_index, turn.line_index
                    );
                    return turn;
                }
            },
        }
    }
}

impl Turn {
    fn is_legal(&self, game: &Game, board_index: usize) -> Result<(), String> {
        match self.factory_index {
            0 => {
                if !game.center.has_tiles() {
                    return Err("Can't select empty center".to_string());
                }
            }
            f_idx @ 1...5 => {
                if !game.factories[(f_idx - 1) as usize].has_tiles() {
                    return Err("Can't select empty factory".to_string());
                }
            }
            _ => return Err("Invalid factory number".to_string()),
        };

        match self.line_index {
            0 => (),
            l_idx @ 1...5 => {
                if game.boards[board_index].lines[(l_idx - 1) as usize].has_other_color(&self.color)
                {
                    return Err(format!(
                        "Line {} already has a different color than {}",
                        l_idx, self.color
                    ));
                };

                for (tile, filled) in game.boards[board_index].wall.tiles[l_idx as usize - 1].iter()
                {
                    if *filled && *tile == self.color {
                        return Err(format!(
                            "Wall line {} already has color {}",
                            l_idx, self.color
                        ));
                    }
                }
            }
            linchar => return Err(format!("Invalid line number: {}", linchar)),
        }

        Ok(())
    }
}

fn parse_chars(game: &Game, line: &str, board_index: usize) -> Result<Turn, String> {
    let mut chars = line.chars().take(3);
    let f_idx = char_to_factory_index(chars.next().unwrap())?;
    let color = char_to_color(chars.next().unwrap())?;
    let line_idx = char_to_line_index(&game, chars.next().unwrap(), &color, board_index)?;
    return Ok(Turn {
        factory_index: f_idx,
        color: color,
        line_index: line_idx,
    });
}

fn char_to_color(c: char) -> Result<Color, String> {
    match c {
        'B' | 'b' => Ok(Color::Blue),
        'G' | 'g' => Ok(Color::Green),
        'K' | 'k' => Ok(Color::Black),
        'R' | 'r' => Ok(Color::Red),
        'Y' | 'y' => Ok(Color::Yellow),
        _ => Err(format!("Invalid color: {}", c)),
    }
}

fn char_to_factory_index(numchar: char) -> Result<usize, String> {
    match numchar.to_digit(10) {
        Some(f_idx @ 0...5) => Ok(f_idx as usize),
        _ => Err("Invalid factory number".to_string()),
    }
}

fn char_to_line_index(
    game: &Game,
    linchar: char,
    color: &Color,
    board_size: usize,
) -> Result<usize, String> {
    match linchar.to_digit(10) {
        Some(0) => Ok(0),
        Some(l_idx @ 1...5) => {
            if game.boards[board_size].lines[(l_idx - 1) as usize].has_other_color(color) {
                return Err(format!(
                    "Line {} already has a different color than {}",
                    l_idx, color
                ));
            };

            for (tile, filled) in game.boards[board_size].wall.tiles[l_idx as usize - 1].iter() {
                if *filled && *tile == *color {
                    return Err(format!("Wall line {} already has color {}", l_idx, color));
                }
            }

            Ok(l_idx as usize)
        }
        _ => Err(format!("Invalid line number: {}", linchar)),
    }
}
