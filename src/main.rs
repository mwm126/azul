use std::fmt;

mod tile;
use tile::Color;

mod bag;
use bag::Bag;

mod factory;
use factory::Factory;

mod board;
use board::Board;

mod turn;
use turn::{read_turn, Turn, read_number_of_players};

pub struct Game {
    bag: Bag,
    factories: Vec<Factory>,
    center: Factory,
    boards: Vec<Board>,
}

impl Game {
    pub fn new(n: u8) -> Self {
        let mut game = Game {
            center: Factory::new(),
            factories: vec![],
            bag: Bag::new(),
            boards: vec![],
        };
        for _ in 1..=5 {
            game.factories.push(Factory::new());
        }
        game.boards.push(Board::new()); // solo player
        if n == 2 {
            game.boards.push(Board::new()); // 2-player
        }
        game
    }

    fn deal(&mut self) {
        for ii in 0..5 {
            for _ in 0..4 {
                match self.bag.pop() {
                    Some(color) => self.factories[ii].push(color),
                    None => {
                        println!("Bag is empty.");
                        return;
                    }
                }
            }
        }
    }

    fn get_factory_tiles(&mut self, turn: &Turn) -> Vec<Color> {
        if turn.factory_index == 0 {
            self.center.drain().collect()
        } else {
            self.factories[turn.factory_index - 1].drain().collect()
        }
    }

    fn score(&mut self) -> bool {
        let mut game_over = false;
        for board in self.boards.iter_mut() {
            game_over = board.score();
        }
        game_over
    }

    fn round_done(&self) -> bool {
        !self.center.has_tiles() && !self.factories.iter().any(|f| f.has_tiles())
    }
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "\n\n * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * \n")?;
        write!(f, "Center  0: {}", self.center)?;
        write!(f, "Factory 1: {}", self.factories[0])?;
        write!(f, "Factory 2: {}", self.factories[1])?;
        write!(f, "Factory 3: {}", self.factories[2])?;
        write!(f, "Factory 4: {}", self.factories[3])?;
        write!(f, "Factory 5: {}", self.factories[4])?;
        writeln!(f, "")?;
        for (index, board) in self.boards.iter().enumerate() {
            write!(f, "\n\n===========================\n")?;
            write!(f, "######## PLAYER  {} #######\n\n{}", index+1, board)?;
            write!(f, "===========================\n\n")?;
        }
        Ok(())
    }
}

fn main() {
    let n = read_number_of_players();
    let mut game = Game::new(n);

    println!("Enter factory number 0-5, (0 is Center) and color [BGKRY] or [bgkry]. For instance, 2y3 is yellow tile from factory 2 to line 3, 0k1 is black tile from center to line 1. 0b0 is move center blue tiles to the floor.");

    while game.bag.not_empty() {
        game.deal();

        let mut center_broken = false;
        'round: loop {
            for board_index in 0..game.boards.len() {
                println!("{}", game);
                let turn = read_turn(&game, board_index);
                if !center_broken && turn.line_index == 0 {
                    game.boards[board_index].floor += 1;
                    center_broken = true;
                };
                for tile in game.get_factory_tiles(&turn) {
                    if tile == turn.color {
                        game.boards[board_index].play(&turn)
                    } else {
                        game.center.push(tile)
                    }
                }
                if game.round_done() {
                    break 'round;
                }
            }
        }
        println!("\n\n\n################## SCORING ##################");
        println!("################## SCORING ##################");
        println!("################## SCORING ##################");
        if game.score() {
            println!("{}", game);
            if game.boards[0].score < game.boards[1].score {
                println!("\n\n################## \n\n Vertical 5, Game Over \n  Congrats Player 2 \n ##################");
            } else if game.boards[0].score > game.boards[1].score {
                println!("\n\n################## \n\n Vertical 5, Game Over \n  Congrats Player 1 \n ##################");
            } else {
                println!("\n\n################## \n\n Vertical 5, Game Over \n  TIE GAME \n ##################");
            }
            break;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::Game;

    #[test]
    fn print_game() {
        let mut game = Game::new();
        game.deal();
        println!("{}", game);
    }
}
