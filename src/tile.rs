use std::fmt;

#[derive(PartialEq, Hash, Clone, Debug)]
pub enum Color {
    Black,
    Blue,
    Green,
    Red,
    Yellow,
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Color::Black => write!(f, "K"),
            Color::Blue => write!(f, "B"),
            Color::Green => write!(f, "G"),
            Color::Red => write!(f, "R"),
            Color::Yellow => write!(f, "Y"),
        }
    }
}
