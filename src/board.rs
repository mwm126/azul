use std::fmt;

use super::tile::Color;
use super::turn::Turn;

pub struct Wall {
    pub tiles: Vec<Vec<(Color, bool)>>,
}

pub struct Board {
    pub wall: Wall,
    pub score: i32,
    pub floor: u8,
    pub lines: Vec<Line>,
}

impl Board {
    pub fn new() -> Self {
        let mut board = Board {
            lines: vec![],
            wall: Wall { tiles: vec![] },
            floor: 0,
            score: 0,
        };
        for ii in 1..=5 {
            board.lines.push(Line {
                color: None,
                fill: 0,
                max: ii,
            });
        }
        board.wall.tiles.push(vec![
            (Color::Blue, false),
            (Color::Yellow, false),
            (Color::Red, false),
            (Color::Black, false),
            (Color::Green, false),
        ]);
        board.wall.tiles.push(vec![
            (Color::Green, false),
            (Color::Blue, false),
            (Color::Yellow, false),
            (Color::Red, false),
            (Color::Black, false),
        ]);
        board.wall.tiles.push(vec![
            (Color::Black, false),
            (Color::Green, false),
            (Color::Blue, false),
            (Color::Yellow, false),
            (Color::Red, false),
        ]);
        board.wall.tiles.push(vec![
            (Color::Red, false),
            (Color::Black, false),
            (Color::Green, false),
            (Color::Blue, false),
            (Color::Yellow, false),
        ]);
        board.wall.tiles.push(vec![
            (Color::Yellow, false),
            (Color::Red, false),
            (Color::Black, false),
            (Color::Green, false),
            (Color::Blue, false),
        ]);
        board
    }

    pub fn play(&mut self, turn: &Turn) {
        if turn.line_index > 0
            && self.lines[turn.line_index - 1].fill < self.lines[turn.line_index - 1].max
        {
            self.lines[turn.line_index - 1].append(turn.color.clone())
        } else {
            self.floor += 1;
        }
    }

    pub fn score(&mut self) -> bool {
        self.score_floor();
        self.tile_wall()
    }

    fn tile_wall(&mut self) -> bool {
        let mut game_over = false;
        for line_idx in 0..5 {
            let mut total_of_this_color = 0;
            if self.lines[line_idx].is_full() {
                self.lines[line_idx].empty();
                let mut consecutive_before = 0;
                let mut scored_column: Option<usize> = None;

                let mut horz_score = 0;
                for (col_index, b) in self.wall.tiles[line_idx].iter_mut().enumerate() {
                    if scored_column.is_some() {
                        if b.1 {
                            horz_score += 1;
                        } else {
                            break;
                        }
                    }
                    if self.lines[line_idx].is_color(&b.0) {
                        b.1 = true;
                        horz_score += consecutive_before + 1;
                        consecutive_before = 0;
                        scored_column = Some(col_index);
                    } else if b.1 {
                        consecutive_before += 1;
                    } else {
                        consecutive_before = 0;
                    }
                    if self.lines[line_idx].is_color(&b.0) {
                        total_of_this_color += 1;
                    }
                }

                let mut scored_row = None;

                let mut vert_score = 0;
                if scored_column.is_some() {
                    let scored_column = scored_column.unwrap();
                    for row_idx in 0..5 {
                        let b = &self.wall.tiles[row_idx][scored_column];

                        if scored_row.is_some() {
                            if b.1 {
                                vert_score += 1;
                            } else {
                                break;
                            }
                        }
                        if row_idx == line_idx {
                            vert_score += consecutive_before + 1;
                            consecutive_before = 0;
                            scored_row = Some(row_idx);
                        } else if b.1 {
                            consecutive_before += 1;
                        } else {
                            consecutive_before = 0;
                        }
                    }
                }

                if vert_score == 5 {
                    vert_score += 7;
                    game_over = true;
                }

                if horz_score == 5 {
                    horz_score += 2;
                }

                if horz_score > 1 && vert_score > 1 {
                    self.score += horz_score + vert_score;
                } else {
                    self.score += horz_score * vert_score;
                }
            }
            if total_of_this_color == 5 {
                self.score += 10;
            }
        }
        game_over
    }

    fn score_floor(&mut self) {
        self.score += match self.floor {
            1...2 => -1,
            3...5 => -2,
            6...7 => -3,
            _ => 0,
        };
        self.floor = 0;
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Game score:  {}\n", self.score)?;
        for ii in 0..5 {
            write!(f, "{} ", self.lines[ii])?;
            for jj in 0..5 {
                match self.wall.tiles[ii][jj] {
                    (ref tile, true) => {
                        write!(f, "{} ", tile)?;
                        ()
                    }
                    (ref tile, false) => {
                        write!(f, "{}", format!("{} ", tile).to_lowercase())?;
                        ()
                    }
                }
            }
            writeln!(f, "")?;
        }
        write!(f, "Floor: ")?;
        for _ in 0..self.floor {
            write!(f, "X")?;
        }
        writeln!(f, "")
    }
}

pub struct Line {
    color: Option<Color>,
    pub fill: usize,
    pub max: usize,
}

impl Line {
    pub fn has_other_color(&self, color: &Color) -> bool {
        match self.color {
            Some(ref c) => c != color,
            None => false,
        }
    }

    fn is_color(&self, color: &Color) -> bool {
        match self.color {
            None => false,
            Some(ref c) => c == color,
        }
    }

    pub fn append(&mut self, color: Color) {
        self.color = Some(color);
        self.fill += 1;
    }

    fn is_full(&self) -> bool {
        self.fill == self.max
    }
    fn empty(&mut self) {
        self.fill = 0;
    }
}

impl fmt::Display for Line {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for _ in 0..self.fill {
            write!(f, "{} ", self.color.clone().unwrap())?;
        }
        for _ in self.fill..self.max {
            write!(f, "_ ")?;
        }
        for _ in self.max..5 {
            write!(f, "  ")?;
        }
        write!(f, "")
    }
}

#[cfg(test)]
mod tests {
    use super::Board;
    use super::Color;

    #[test]
    fn a1() {
        let mut b = Board::new();
        b.lines[0].fill = b.lines[0].max;
        b.lines[0].color = Some(Color::Blue);
        b.tile_wall();
        assert_eq!(b.score, 1);
    }

    #[test]
    fn b2() {
        let mut b = Board::new();
        b.lines[1].fill = b.lines[1].max;
        b.lines[1].color = Some(Color::Blue);
        b.tile_wall();
        assert_eq!(b.score, 1);
    }

    #[test]
    fn b2c2() {
        let mut b = Board::new();
        b.lines[1].fill = b.lines[1].max;
        b.lines[1].color = Some(Color::Blue);
        b.lines[2].fill = b.lines[2].max;
        b.lines[2].color = Some(Color::Green);
        b.tile_wall();
        assert_eq!(b.score, 3);
    }
}
