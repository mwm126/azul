use std::fmt;
use std::vec::Drain;

use super::tile::Color;

pub struct Factory {
    tiles: Vec<Color>,
}

impl Factory {
    pub fn new() -> Self {
        Factory { tiles: vec![] }
    }

    pub fn has_tiles(&self) -> bool {
        return !self.tiles.is_empty();
    }

    pub fn push(&mut self, color: Color) {
        self.tiles.push(color)
    }

    pub fn drain(&mut self) -> Drain<Color> {
        self.tiles.drain(0..)
    }
}

impl fmt::Display for Factory {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for tile in self.tiles.iter() {
            write!(f, "{}", tile)?;
        }
        writeln!(f, "")?;
        Ok(())
    }
}
