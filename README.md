# Azul in Rust

This is an implementation of the board game Azul in Rust, implemented as an exercise.

## Build

Install [[Rust | http://rustup.rs]]

```
cargo run
```
